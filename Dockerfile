# Start FROM Python 3.8 Image
FROM python:3.8

# Install python dependencies
COPY requirements.txt .
RUN python -m pip install --upgrade pip
RUN pip install --no-cache -r requirements.txt
RUN pip install -r requirements.txt

# Create working directory
RUN mkdir -p usr/src/app
RUN mkdir -p usr/src/app/target
WORKDIR /usr/src/app

# Copy contents
COPY . /usr/src/app

# Set environment variables
ENV HOME=/usr/src/app
