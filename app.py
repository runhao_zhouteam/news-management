import os
import sys
import time

from colorama import Fore, Style, init

from getpass import getpass
from news_management.service.user_service import UserService
from news_management.service.news_service import NewsService
from news_management.service.role_service import RoleService

init()
__user_service = UserService()
__news_service = NewsService()
__role_service = RoleService()

while True:
    os.system("cls")
    print(Fore.LIGHTBLUE_EX, "\n\t==================")
    print(Fore.LIGHTBLUE_EX, "\n\tWelcome to the News Management System")
    print(Fore.LIGHTBLUE_EX, "\n\t==================")
    print(Fore.LIGHTGREEN_EX, "\n\t1.Log in")
    print(Fore.LIGHTGREEN_EX, "\n\t2.Exit")
    print(Style.RESET_ALL)
    opt = input("\n\tEnter operation number:")
    if opt == "1":
        username = input("\n\tUsername:")
        password = getpass("\n\tPassword:")
        result = __user_service.login(username, password)
        # Log in successfully
        if result == True:
            # Check User
            role = __user_service.search_user_role(username)
            while True:
                os.system("cls")
                if role == "News Editor":
                    print('test')
                elif role == "Admin":
                    print(Fore.LIGHTGREEN_EX, "\n\t1.News Management")
                    print(Fore.LIGHTGREEN_EX, "\n\t2.User Management")
                    print(Fore.LIGHTRED_EX, "\n\tback.Log out")
                    print(Fore.LIGHTRED_EX, "\n\texit.Exit")
                    print(Style.RESET_ALL)
                    opt = input("\n\tEnter operation number:")
                    if opt == "1":
                        while True:
                            os.system("cls")
                            print(Fore.LIGHTGREEN_EX, "\n\t1.Approval News")
                            print(Fore.LIGHTGREEN_EX, "\n\t2.Delete News")
                            print(Fore.LIGHTRED_EX, "\n\tback.Back to previous level")
                            print(Style.RESET_ALL)
                            opt = input("\n\tEnter operation number:")
                            if opt == "1":
                                page = 1
                                while True:
                                    os.system("cls")
                                    count_page = __news_service.search_unreview_count_page()
                                    result = __news_service.search_unreview_list(page)
                                    for index in range(len(result)):
                                        one = result[index]
                                        print(Fore.LIGHTBLUE_EX,
                                              "\n\t%d\t%s\t%s\t%s" % (index + 1, one[1], one[2], one[3]))
                                    print(Fore.LIGHTBLUE_EX, "\n\t-------------------")
                                    print(Fore.LIGHTBLUE_EX, "\n\t%d/%d" % (page, count_page))
                                    print(Fore.LIGHTBLUE_EX, "\n\t-------------------")
                                    print(Fore.LIGHTRED_EX, "\n\tback.Back to previous level")
                                    print(Fore.LIGHTRED_EX, "\n\tprev.Previous page")
                                    print(Fore.LIGHTRED_EX, "\n\tnext.Next page")
                                    print(Style.RESET_ALL)
                                    opt = input("\n\tEnter operation number:")
                                    if opt == "back":
                                        break
                                    elif opt == "prev" and page > 1:
                                        page -= 1
                                    elif opt == "next" and page < count_page:
                                        page += 1
                                    elif int(opt) >= 1 and int(opt) <= 10:
                                        news_id = result[int(opt) - 1][0]
                                        __news_service.update_unreview_news(news_id)
                            elif opt == "2":
                                page = 1
                                while True:
                                    os.system("cls")
                                    count_page = __news_service.search_count_page()
                                    result = __news_service.search_list(page)
                                    for index in range(len(result)):
                                        one = result[index]
                                        print(Fore.LIGHTBLUE_EX,
                                              "\n\t%d\t%s\t%s\t%s" % (index + 1, one[1], one[2], one[3]))
                                    print(Fore.LIGHTBLUE_EX, "\n\t-------------------")
                                    print(Fore.LIGHTBLUE_EX, "\n\t%d/%d" % (page, count_page))
                                    print(Fore.LIGHTBLUE_EX, "\n\t-------------------")
                                    print(Fore.LIGHTRED_EX, "\n\tback.Back to previous level")
                                    print(Fore.LIGHTRED_EX, "\n\tprev.Previous page")
                                    print(Fore.LIGHTRED_EX, "\n\tnext.Next page")
                                    print(Style.RESET_ALL)
                                    opt = input("\n\tEnter operation number:")
                                    if opt == "back":
                                        break
                                    elif opt == "prev" and page > 1:
                                        page -= 1
                                    elif opt == "next" and page < count_page:
                                        page += 1
                                    elif int(opt) >= 1 and int(opt) <= 10:
                                        news_id = result[int(opt) - 1][0]
                                        __news_service.delete_by_id(news_id)
                            elif opt == "back":
                                break
                    elif opt == "2":
                        while True:
                            os.system("cls")
                            print(Fore.LIGHTGREEN_EX, "\n\t1.Add User")
                            print(Fore.LIGHTGREEN_EX, "\n\t2.Modify User")
                            print(Fore.LIGHTGREEN_EX, "\n\t3.Delete User")
                            print(Fore.LIGHTRED_EX, "\n\tback.Back to previous level")
                            print(Style.RESET_ALL)
                            opt = input("\n\tEnter operation number:")
                            if opt == "back":
                                break
                            elif opt == "1":
                                os.system("cls")
                                username = input("\n\tUsername:")
                                password = getpass("\n\tPassword:")
                                repassword = getpass("\n\tRepeat password:")
                                if password != repassword:
                                    print("\n\tInconsistent passwords (3 seconds auto back)")
                                    time.sleep(3)
                                    continue
                                email = input("\n\tEmail:")
                                result = __role_service.search_list()
                                for index in range(len(result)):
                                    one = result[index]
                                    print(Fore.LIGHTBLUE_EX, "\n\t%d.%s" % (index + 1, one[1]))
                                print(Style.RESET_ALL)
                                opt = input("\n\tRole Number:")
                                role_id = result[int(opt) - 1][0]
                                __user_service.insert(username, password, email, role_id)
                                print("\n\tSave successful (3 seconds to back automatically)")
                                time.sleep(3)
                            elif opt == "2":
                                page = 1
                                while True:
                                    os.system("cls")
                                    count_page = __user_service.search_count_page()
                                    result = __user_service.search_list(page)
                                    for index in range(len(result)):
                                        one = result[index]
                                        print(Fore.LIGHTBLUE_EX,
                                              "\n\t%d\t%s\t%s" % (index + 1, one[1], one[2]))
                                    print(Fore.LIGHTBLUE_EX, "\n\t-------------------")
                                    print(Fore.LIGHTBLUE_EX, "\n\t%d/%d" % (page, count_page))
                                    print(Fore.LIGHTBLUE_EX, "\n\t-------------------")
                                    print(Fore.LIGHTRED_EX, "\n\tback.Back to previous level")
                                    print(Fore.LIGHTRED_EX, "\n\tprev.Previous page")
                                    print(Fore.LIGHTRED_EX, "\n\tnext.Next page")
                                    print(Style.RESET_ALL)
                                    opt = input("\n\tEnter operation number:")
                                    if opt == "back":
                                        break
                                    elif opt == "prev" and page > 1:
                                        page -= 1
                                    elif opt == "next" and page < count_page:
                                        page += 1
                                    elif int(opt) >= 1 and int(opt) <= 10:
                                        os.system("cls")
                                        user_id = result[int(opt) - 1][0]
                                        username = input("\n\tNew User:")
                                        password = getpass("\n\tNew Passowrd:")
                                        repassword = getpass("\n\tEnter your password again:")
                                        if password != repassword:
                                            print(Fore.LIGHTRED_EX, "\n\tInconsistent passwords (3 seconds auto back)")
                                            print(Style.RESET_ALL)
                                            time.sleep(3)
                                            break
                                        email = input("\n\tNew Email:")
                                        result = __role_service.search_list()
                                        for index in range(len(result)):
                                            one = result[index]
                                            print(Fore.LIGHTBLUE_EX, "\n\t%d.%s" % (index + 1, one[1]))
                                        print(Style.RESET_ALL)
                                        opt = input("\n\tRole Number:")
                                        role_id = result[int(opt) - 1][0]
                                        opt = input("\n\tSave or not (Y/N)")
                                        if opt == "Y" or opt == "y":
                                            __user_service.update(user_id, username, password, email, role_id)
                                            print("\n\tSave successful (3 seconds to back automatically)")
                                            time.sleep(3)
                            elif opt == "3":
                                page = 1
                                while True:
                                    os.system("cls")
                                    count_page = __user_service.search_count_page()
                                    result = __user_service.search_list(page)
                                    for index in range(len(result)):
                                        one = result[index]
                                        print(Fore.LIGHTBLUE_EX,
                                              "\n\t%d\t%s\t%s" % (index + 1, one[1], one[2]))
                                    print(Fore.LIGHTBLUE_EX, "\n\t-------------------")
                                    print(Fore.LIGHTBLUE_EX, "\n\t%d/%d" % (page, count_page))
                                    print(Fore.LIGHTBLUE_EX, "\n\t-------------------")
                                    print(Fore.LIGHTRED_EX, "\n\tback.Back to previous level")
                                    print(Fore.LIGHTRED_EX, "\n\tprev.Previous page")
                                    print(Fore.LIGHTRED_EX, "\n\tnext.Next page")
                                    print(Style.RESET_ALL)
                                    opt = input("\n\tEnter operation number:")
                                    if opt == "back":
                                        break
                                    elif opt == "prev" and page > 1:
                                        page -= 1
                                    elif opt == "next" and page < count_page:
                                        page += 1
                                    elif int(opt) >= 1 and int(opt) <= 10:
                                        os.system("cls")
                                        user_id = result[int(opt) - 1][0]
                                        __user_service.delete_by_id(user_id)
                                        print("\n\tDeletion successful (3 seconds to return automatically)")
                                        time.sleep(3)

                    if opt == 'back':
                        break
                    elif opt == 'exit':
                        sys.exit(0)

        else:
            print("\n\tLogin failure (3 seconds to back automatically)")
            time.sleep(3)

    elif opt == "2":
        sys.exit(0)
