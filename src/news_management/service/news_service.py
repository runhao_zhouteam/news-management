from news_management.db.news_dao import NewsDao

class NewsService:
    __news_dao = NewsDao()

    #Check the list of pending news
    def search_unreview_list(self, page):
        result = self.__news_dao.search_unreview_list(page)
        return result

    #Check the total number of pages of news to be approved
    def search_unreview_count_page(self):
        count_page = self.__news_dao.search_unreview_count_page()
        return count_page

    #Approval News
    def update_unreview_news(self, id):
        self.__news_dao.update_unreview_news(id)

    #Check the news list
    def search_list(self, page):
        result = self.__news_dao.search_list(page)
        return result

    #Check the total number of news pages
    def search_count_page(self):
        count_page = self.__news_dao.search_count_page()
        return count_page

    #Delete News
    def delete_by_id(self, id):
        self.__news_dao.delete_by_id(id)