from news_management.db.role_dao import RoleDao

class RoleService:
    __role_dao = RoleDao()

    #Check Role List
    def search_list(self):
        result = self.__role_dao.search_list()
        return result