# News Management

News Management System for the new User add, modify and delete.

## Setup development environment

```bash
python3 -m venv .venv
source .venv/bin/activate
pip install -e .
pip install -e .[testing]
pip install -U setuptools setuptools_scm wheel
pre-commit install
pre-commit autoupdate
```

## Docker

Here is env variables, user can change variable in ***.env*** file

```txt
CONTAINER_VOLUME=/usr/src/app/target# the volume path in container
HOST_DIR=./data						# the host directory in host PC
TZ=Europe/Amsterdam					# Timezone
SERVER_SSL_ENABLED=true
PYTHONUNBUFFERED=0		
```

Then to start the server via docker, please run the following commands:

```bash
cd news_management
docker-compose up
```

## Workflow and Versioning

### Workflow

1. Development on branch
1. Regular commits
1. When feature is ready: start merge request

Please follow the agreed [Definition of Done](https://wissen.ivi.fraunhofer.de/display/OE221/Students#Students-DefinitionofDone-DOD)!

### Versioning

Every time an important change has been made, add an git-tag  major.minor.fix using [semantic versioning](https://semver.org/).

```bash
git tag -a 0.1.0 -m 'version 0.1.0 - With feature XYZ'
```

## Testing and code guidelines

### Checks before commit

Execute pytests and pre-commit hooks before committing

```bash
make check
```

### Checks after commit

Test pipelines are triggered automatically from the gitlab-ci for every commit.
Monitor the test result and act on any failure of them.

## References
