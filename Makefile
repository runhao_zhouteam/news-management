check:
	pre-commit run --all-files
	pytest
	coverage report --fail-under=80
